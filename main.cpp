#include <string>
#include <iostream>
using namespace std;

int CountWords(string str)
{
   bool inSpaces = true;
   int numWords = 0;
   
   for (char& c : str)
   {
      if (c == ' ')
      {
         inSpaces = true;
      }
      else if (inSpaces)
      {
         numWords++;
         inSpaces = false;
      }
   }

   return numWords;
}

int main()
{
    cout<<"Введите строку в которой необходимо посчитать слова:" << endl;

    string str;
    getline(cin, str);
    
    cout << "Количество слов в строке: " << CountWords(str) << endl;

    return 0;
}

